# Employee Management System

Welcome to the Employee Management System! This Spring Boot application, coupled with Thymeleaf templates, empowers you to perform CRUD (Create, Read, Update, Delete) operations seamlessly for managing employee records stored in a MySQL database.

## Features

- **Create**: Easily add new employee records.
- **Read**: Retrieve and view existing employee details.
- **Update**: Modify employee information as needed.
- **Delete**: Remove employee records efficiently.

## Technologies Used

- **Spring Boot**: Offers a powerful framework for rapid web application development.
- **Thymeleaf**: Simplifies the creation of dynamic HTML templates, enhancing user experience.

## Getting Started

To get started with the Employee Management System, follow these steps:

1. Ensure you have MySQL installed on your system.
2. Clone this repository to your local machine.
3. Set up the MySQL database using the provided SQL scripts.
4. Navigate to the project directory.
5. Run the application using `mvn spring-boot:run`.
6. Access the application at [http://localhost:8080](http://localhost:8080).

## Usage

- **Home Page**: Visit [http://localhost:8080](http://localhost:8080) to access the dashboard displaying a summary of available employees.
- **Add New Employee**: Go to [http://localhost:8080/showNewEmployeeForm](http://localhost:8080/showNewEmployeeForm) to save new employee details.
- **Edit Employee Details**: Access [http://localhost:8080/showFormForUpdate/{id}](http://localhost:8080/showFormForUpdate/{id}) to modify employee information.
- **Delete Employee Record**: Utilize [http://localhost:8080/deleteEmployee/{id}](http://localhost:8080/deleteEmployee/{id}) to remove employee records from the database.

## Support

For any inquiries or assistance, feel free to reach out to our support team at [kkalyankumar25@gmail.com](mailto:kkalyankumar25@gmail.com).

Thank you for choosing the Employee Management System! We hope it simplifies your employee management tasks effectively.
